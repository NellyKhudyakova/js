export class Sensor {
  id: number;
  name: string;
  status: boolean;

  constructor(id, name, status) {
    this.id = id;
    this.name = name;
    this.status = status;
  }
}
