import {Component, OnInit} from '@angular/core';
import {Sensor} from "./shared/models/sensor.model";

let index = 1;
const generateRandomSensor = () => new Sensor(index, `sensor${index++}`, Math.random() > 0.5);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'Список датчиков';
  arr: Sensor[] = [];

  constructor() {
    for (let i = 0; i < 10; i++) {
      this.arr.push(generateRandomSensor())
    }
  }

  ngOnInit() {

  }

  sensor_name: string;
  sensor_status: string = 'online';

  AddSensor() {

    let newSensor = generateRandomSensor();
    newSensor.name = this.sensor_name;
    newSensor.status = (this.sensor_status === 'online');

    console.log(newSensor.name, newSensor.status);
    this.arr.push(newSensor);
  }

  DeleteSensor(i) {
    this.arr.splice(i,1);
  }
}
